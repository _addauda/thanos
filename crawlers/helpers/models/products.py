class Product:

	def __init__(self, name, price, unit, product_info, product_picture, url):

		self.unit = unit
		self.name = name
		self.price = price
		self.product_info = product_info
		self.product_picture = product_picture
		self.url = url

