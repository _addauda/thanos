import re
import os
import json
import datetime
import csv
import requests
import sys
import logging
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from itertools import groupby
from crawlers.helpers.models.products import Product
import pytz
tz = pytz.timezone('America/Toronto')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class NoFrills:

	def __init__(self, log_dir=None):
		
		self.configure_logging(log_dir)
		logger.info('initializing class:%s', __name__)

		self.store_name = 'No Frills'
		self.store_slug = __name__
		self.store_abbr = 'NF'
		self.url = 'https://www.nofrills.ca/search/?search-bar='
		self.product_name_tag = 'product-name__item--name'
		self.product_page_tag = 'product-tile-group'
		self.all_products_tag = 'product-tile-group__item'
		self.url_tag = 'product-details-link'
		self.logger = logger
		self.products = []

	def configure_logging(self, log_dir):
		now = datetime.datetime.now(tz=tz)
		logger.info('configuring logging for class:%s log_dir:%s', __name__, log_dir)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s","%Y-%m-%d %H:%M:%S")
		file_handler = logging.FileHandler(log_dir + __name__ + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.log')
		file_handler.setLevel(logging.INFO)
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)

	def clean_string(self, dirty_string):
		#return dirty_string.encode('utf-8').strip()
		return dirty_string.strip()

	def crawl(self,product_keywords):

		try:
			start_time = time.time()

			logger.info('starting crawler for store:%s', self.store_slug)

			for key in product_keywords:
				try:
					current_product = product_keywords[key][self.store_abbr]
					logger.info('processing product:%s for store:%s', key, self.store_slug)
				except:
					logger.warn('skipping product:%s for store:%s', key, self.store_slug)
					continue

				## Selenium set-up
				options = webdriver.ChromeOptions()
				options.add_argument('window-size=800x841')
				options.add_argument('headless')
				options.add_argument('--no-sandbox')
				options.add_argument('--disable-dev-shm-usage')
				driver = webdriver.Chrome(chrome_options=options)

				request_url = self.url + current_product
				page = driver.get(request_url)
				#driver.find_element_by_name("Ontario")
				logger.info('requesting content at url:%s', request_url)

				## Parsing page from selenium
				soup = BeautifulSoup(driver.page_source, 'html.parser')
				product_page = soup.find('ul', class_=self.product_page_tag)
				try:
					master_list = product_page.findAll('li',class_=self.all_products_tag)
				except:
					logger.exception('Unable to retrive product list at %s', request_url)
					continue

				for product in master_list:

					## Extracting product name
					name = self.clean_string(product.find('span',class_=self.product_name_tag).get_text())
						
					## Extracting product price
					try: 
						price = product.find('span',class_='price__value comparison-price-list__item__price__value').get_text()
						unit = product.find('span',class_='price__unit comparison-price-list__item__price__unit').get_text()
					except:
						price = product.find('span',class_='price__value selling-price-list__item__price selling-price-list__item__price--now-price__value').get_text()
						unit = product.find('span',class_='price__unit selling-price-list__item__price selling-price-list__item__price--now-price__unit').get_text()
						pass

					## Parsing price
					price_computation = price.replace('$','')
					unit_array = [''.join(x) for _, x in groupby(unit.replace('/ ',''), key=str.isdigit)]
					weight = unit_array[0]
					unit = unit_array[(len(unit_array)-1)]
					logger.info('parsed raw price:%s raw weight:%s raw unit:%s', price, weight, unit)

					## Conversion to imperial units, if possible
					if (unit == 'kg'):
						## When unit weight is 1 kg
						if (weight == 'kg'):
							price = round((float(price_computation)/float(1))*(1/2.20462),2)
							unit = 'lb'
							weight = 1
						else:
							price = round((float(price_computation)/float(weight))*(1/2.20462),2)
							unit = 'lb'
							weight = 1
					elif (unit == 'g'):
						if (weight == 'g'):
							price = round((float(price_computation)/float(1))*(1/0.00220462),2)
							unit = 'lb'
							weight = 1
						else:
							price = round((float(price_computation)/float(weight))*(1/0.00220462),2)
							unit = 'lb'
							weight = 1
					else:
						price = float(self.clean_string(price_computation))
		
					## Parse product URL
					url = 'https://www.nofrills.ca/' + str(product.find('a', class_='product-tile__details__info__name__link')['href'])
					logger.info('found product url:%s', url)  

					## Parse product information
					try:
						driver.get(url)
						product_page = BeautifulSoup(driver.page_source,"html.parser")
						product_info = product_page.find("div",class_= "product-description-text").get_text().strip()
					except:
						#logger.info('Unable to retrive product description at %s', url)
						logger.exception('Unable to retrive product description at %s', url)
						product_info = "No product information provided"
						pass
					
					## Product picture url
					try:
						product_picture = product_page.find("img",class_="responsive-image--product-details-page")["src"]
					except:
						logger.exception("Unable to retrive product image at %s", url)
						product_picture = "No product image provided"
						pass
						
					## Adds newly crawled product to list
					if (weight == unit):
						item = Product(name, price, unit, product_info, product_picture, url)
					else:
						item = Product(name, price, str(weight) + ' ' + unit, product_info, product_picture, url)
					self.products.append(item)
					logger.info('formatted product as name:%s price:%s weight:%s unit:%s', item.name, item.price, weight, item.unit)
		
		except:
			logger.exception('ERROR occurred trying to crawl keyword:' + key)
			pass
		
		finally:
			elapsed_time = time.time() - start_time
			logger.info('runtime:%s', time.strftime('%H:%M:%S', time.gmtime(elapsed_time)))
		
	def write_csv(self, output_dir):
		now = datetime.datetime.now(tz=tz)
		file_name = output_dir + self.store_slug + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.csv'

		with open(file_name, mode='w') as output_file:
			output_file = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			output_file.writerow(["name", "price", "unit", "product_info", "product_image", "url"])

			for product in self.products:
				output_file.writerow([product.name, product.price, product.unit, product.product_info, product.product_picture, product.url])
		return file_name

	def write_workbook(self, workbook):
		worksheet = workbook.add_worksheet(self.store_slug)

		## Add a bold format to use to highlight cells.
		bold = workbook.add_format({'bold': 1})

		## Write some data headers.
		worksheet.write('A1', 'name', bold)
		worksheet.write('B1', 'price', bold)
		worksheet.write('C1', 'unit', bold)
		worksheet.write('D1', 'product_info', bold)
		worksheet.write('E1', 'product_image', bold)
		worksheet.write('F1', 'product_url', bold)

		 # Start from the first cell below the headers.
		row = 1
		col = 0

		for product in self.products:
			worksheet.write_string(row, col, product.name)
			worksheet.write_number(row, col + 1, product.price)
			worksheet.write_string(row, col + 2, product.unit)
			worksheet.write_string(row, col + 3, product.product_info)
			worksheet.write_string(row, col + 4, product.product_picture)
			worksheet.write_string(row, col + 5, product.url)
			row += 1
