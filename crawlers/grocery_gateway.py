import csv
from bs4 import BeautifulSoup
import requests
from crawlers.helpers.levenshtein import ratio
from crawlers.helpers.models.products import Product
from itertools import groupby
import re
import datetime
import logging
import sys
import time
import pytz
tz = pytz.timezone('America/Toronto')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class GroceryGateway:

	def __init__(self, log_dir=None):

		self.configure_logging(log_dir)
		logger.info('initializing class:%s', __name__)

		self.store_name = 'Grocery Gateway'
		self.store_slug = __name__
		self.store_abbr = 'GG'
		self.url = 'https://www.grocerygateway.com/store/groceryGateway/en/search/?text=' 
		self.product_list_tag = 'js-quickview-wrapper'
		self.product_tag = 'gtm-product'
		self.name_tag ='product-card__name'
		self.url_tag = 'product-card__content'
		self.product_price_tag = 'price'
		self.product_detail = 'rating-bottom'
		self.product_image_tag = 'medias-slider__media'
		self.products = []

	def configure_logging(self, log_dir):
		now = datetime.datetime.now(tz=tz)
		logger.info('configuring logging for class:%s log_dir:%s', __name__, log_dir)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s","%Y-%m-%d %H:%M:%S")
		file_handler = logging.FileHandler(log_dir + __name__ + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.log')
		file_handler.setLevel(logging.INFO)
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)

	def clean_string(self, dirty_string):
		#return dirty_string.encode('utf-8').strip()
		return dirty_string.strip()


	def crawl(self, product_keywords):

			try:
				start_time = time.time()

				logger.info('starting crawler for store:%s', self.store_slug)

				for key in product_keywords:
					## Check to see store_abbr is defined for current product
					try:
						current_product = product_keywords[key][self.store_abbr]
						logger.info('processing product:%s for store:%s', key, self.store_slug)
					except:
						logger.warn('skipping product:%s for store:%s', key, self.store_slug)
						continue
					
					request_url = self.url + current_product
					page = requests.get(request_url)
					logger.info('requesting content at url:%s', request_url)

					soup = BeautifulSoup(page.content, 'html.parser')

					product_section = soup.find('ul', class_=self.product_list_tag)
					master_list = product_section.findAll('li', class_=self.product_tag)

					for listing in master_list:

						name = self.clean_string(listing.find('a', class_=self.name_tag).get_text())

						## Retrieve product url
						url_tag = listing.find('div', class_=self.url_tag)
						url = 'https://www.grocerygateway.com' + str(url_tag.find(href=True)['href'])
						logger.info('found product url: %s',url)

						## Retrieve product page
						product_page = requests.get(url)
						product_soup = BeautifulSoup(product_page.content,'html.parser')

						## Retrieve product image
						product_picture = product_soup.find('div', class_= self.product_image_tag)['data-src']
						
						## Retrieve product description
						product_info = ""
						try:
							product_info = product_soup.find('div', class_='about-info-section').get_text()
							product_info += product_soup.find('div', class_='description').get_text()
						except:
							pass
			
						## Retrieve product price
						product_price_class = product_soup.find('div', class_= self.product_detail)

						## Special case where fruit has a range of weights (Example: $2.20 / 150-200g)
						try:
							price_and_unit = product_price_class.find('span', class_='actualPrice').get_text().replace('(','').replace(')','')
							price_and_unit_array = price_and_unit.rstrip().strip().split('/')
							price = self.clean_string(price_and_unit_array[0])
							unit = price_and_unit_array[(len(price_and_unit_array) - 1)]
							weight = 1
						
						## Regular case
						except:

							price = self.clean_string(product_price_class.find('span', class_=self.product_price_tag).get_text())

							## Parsing product price
							price_to_convert = re.sub('\s+', ' ', price + product_soup.find('span', class_='unitSize').get_text().rstrip())
							price_and_unit_array = price_to_convert.replace('$','').replace(' ','').split('/')
							formatted_price = price.replace('$','')

							## Parsing product unit and weight
							if "x" in (price_and_unit_array[(len(price_and_unit_array)-1)]):
								unit = price_and_unit_array[(len(price_and_unit_array)-1)]
								weight = ""
								pass
							else:
								weight_array = price_and_unit_array[(len(price_and_unit_array) - 1)]
								weight_and_unit = [''.join(x) for _, x in groupby(weight_array, key=str.isdigit)]
								weight = weight_and_unit[0]
								unit = weight_and_unit[(len(weight_and_unit) - 1)]
								pass 

						logger.info('parsed product as raw name:%s raw price:%s raw weight:%s raw unit:%s', name, price, weight, unit)
						
						## Converts to imperial units when possible
						if (unit == 'kg'):
							## When unit weight is 1 kg
							if (weight == 'kg'):
								price = round((float(formatted_price) / float(1)) * (1 / 2.20462), 2)
								unit = 'lb'
								weight = 1
							else:
								price = round((float(formatted_price) / float(weight)) * (1 / 2.20462), 2)
								unit = 'lb'
								weight = 1
						elif (unit == 'g'):
							if (weight == 'g'):
								price = round((float(formatted_price) / float(1)) * (1 / 0.00220462), 2)
								unit = 'lb'
								weight = 1
							else:
								price = round((float(formatted_price) / float(weight)) * (1 / 0.00220462), 2)
								unit = 'lb'
								weight = 1
						else:
							price = float(self.clean_string(formatted_price))
						
						## Adds newly crawled product to list
						if (weight == unit):
							item = Product(name, price, unit, product_info, product_picture, url)
						else:
							item = Product(name, price, str(weight) + " " + unit, product_info, product_picture, url)
						self.products.append(item)
						logger.info('formatted product as name:%s price:%s weight:%s unit:%s', item.name, item.price, weight, item.unit)
					
			## Exception handling, logs problem url.
			except:
				logger.exception('ERROR occurred trying to crawl keyword:' + key)
				pass
			
			finally:
				elapsed_time = time.time() - start_time
				logger.info('runtime:%s', time.strftime('%H:%M:%S', time.gmtime(elapsed_time)))
		
	def write_csv(self, output_dir):
		now = datetime.datetime.now(tz=tz)
		file_name = output_dir + self.store_slug + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.csv'
		with open(file_name, mode='w') as output_file:
			output_file = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			output_file.writerow(["name", "price", "unit", "product_info", "product_image", "url"])
			for product in self.products:
				output_file.writerow([product.name, product.price, product.unit, "None", product.product_picture, product.url])
	
	def write_workbook(self, workbook):
		worksheet = workbook.add_worksheet(self.store_slug)

		## Add a bold format to use to highlight cells.
		bold = workbook.add_format({'bold': 1})

		## Write some data headers.
		worksheet.write('A1', 'name', bold)
		worksheet.write('B1', 'price', bold)
		worksheet.write('C1', 'unit', bold)
		worksheet.write('D1', 'product_info', bold)
		worksheet.write('E1', 'product_image', bold)
		worksheet.write('F1', 'product_url', bold)

		 # Start from the first cell below the headers.
		row = 1
		col = 0

		for product in self.products:
			worksheet.write_string(row, col, product.name)
			worksheet.write_number(row, col + 1, product.price)
			worksheet.write_string(row, col + 2, product.unit)
			worksheet.write_string(row, col + 3, product.product_info)
			worksheet.write_string(row, col + 4, product.product_picture)
			worksheet.write_string(row, col + 5, product.url)
			row += 1
