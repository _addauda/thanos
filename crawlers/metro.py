import csv
import requests
import logging
import json
import datetime
import re
import sys
import time
from bs4 import BeautifulSoup
from itertools import groupby
from crawlers.helpers.models.products import Product
import pytz
tz = pytz.timezone('America/Toronto')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class Metro:

	def __init__(self, log_dir=None):

		self.configure_logging(log_dir)
		logger.info('initializing class:%s', __name__)

		self.store_name = 'Metro'
		self.store_slug = __name__
		self.store_abbr = 'ME'
		self.name_tag = 'pt--title'
		self.product_page_tag = 'products-listing searchOnlineResults row gutters-small'
		self.products_tag = 'col-auto'
		self.price_tag = '.pi--secondary-price'
		self.product_details_link_tag = 'product-details-link'
		self.url = 'https://www.metro.ca/en/online-grocery/search?filter='
		self.product_picture_tag = 'data-desk-img-src'
		self.product_information_tag = '.accordion--text'
		self.products = []

	def configure_logging(self, log_dir):
		now = datetime.datetime.now(tz=tz)
		logger.info('configuring logging for class:%s log_dir:%s', __name__, log_dir)
		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s","%Y-%m-%d %H:%M:%S")
		file_handler = logging.FileHandler(log_dir + __name__ + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.log')
		file_handler.setLevel(logging.INFO)
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)

	def clean_string(self, dirty_string):
		#return dirty_string.encode('utf-8').strip()
		return dirty_string.strip()

	def crawl(self, product_keywords):

		try:
			start_time = time.time()

			logger.info('starting crawler for store:%s', self.store_slug)
			
			for key in product_keywords:
				try:
					current_product = product_keywords[key][self.store_abbr]
					logger.info('processing product:%s for store:%s', key, self.store_slug)
				except:
					logger.warn('skipping product:%s for store:%s', key, self.store_slug)
					continue

				request_url = self.url + current_product + '&freeText=true'
				page = requests.get(request_url)
				logger.info('requesting content at url:%s', request_url)
				
				soup = BeautifulSoup(page.content, 'html.parser')
				product_page = soup.find(class_=self.product_page_tag)
				master_list = product_page.findAll('div',class_=self.products_tag)

				for product in master_list:
					try:
						name = self.clean_string(product.find('div',class_=self.name_tag).get_text())
					except:
						continue

					## Retrieve product url
					url_class = product.find('a', class_=self.product_details_link_tag)
					product_url = 'https://www.metro.ca' + url_class['href']
					logger.info('found product url:%s', product_url)

					## Parse metric price
					try:
						## metric_price_array = product.select(self.price_tag)
						metric_price_array = product.findAll('span', class_='pi--price')
						metric_price_text = metric_price_array[(len(metric_price_array)-1)].get_text()
						metric_price_unit = metric_price_text.replace('\n','').split('/')
					except:
						metric_price_text = product.find('div', class_='pi--main-price').get_text()

					## Parse product url
					try:
						weight_and_unit = [''.join(x) for _, x in groupby(metric_price_unit[1], key=str.isdigit)]
						price = float(self.clean_string(metric_price_unit[0].replace('$','')))
						unit = weight_and_unit[(len(weight_and_unit)-1)]
						weight = weight_and_unit[0]

					## Except clause used to handle 'buy 2 for $5 sale exceptions'  
					## Price as advertised reported, no parsing or conversion.  
					except:
						price = float(self.clean_string(metric_price_text.replace('$','')))
						unit = ''
						pass

					logger.info('parsed product as raw name:%s raw price:%s raw weight:%s raw unit:%s', name, price, weight, unit)

					## Converts to imperial units when possible
					if (unit == 'kg'):
						## When unit weight is 1 kg
						if (weight == 'kg'):
							price = round((float(price)/float(1))*(1/2.20462),2)
							weight = 1
							unit = 'lb'
						else:
							price = round((float(price)/float(weight))*(1/2.20462),2)
							weight = 1
							unit = 'lb'
					elif (unit == 'g'):
						if (weight == 'g'):
							price = round((float(price)/float(1))*(1/0.00220462),2)
							weight = 1
							unit = 'lb'
						else:
							price = round((float(price)/float(weight))*(1/0.00220462),2)
							weight = 1
							unit = 'lb'
					
					## Retrives product image
					try:
						specifc_product_page = requests.get(product_url)
						specific_product_page_html = BeautifulSoup(specifc_product_page.content,"html.parser")
						picture = "https:" + specific_product_page_html.find(class_="pi--main-img").find("source",id="desk-img")['srcset']
					except:
						picture = "None"
						pass

					## Adds newly crawled product to list
					if (weight == unit):
						item = Product(name, price, unit, "", "", product_url)
					else:
						item = Product(name, price, str(weight) + ' ' + unit, "", picture, product_url)
					self.products.append(item)
					logger.info('formatted product as name:%s price:%s weight:%s unit:%s', item.name, item.price, weight, item.unit)

		except:
			logger.exception('ERROR occurred trying to crawl keyword:' + key)
			pass
			
		finally:
			elapsed_time = time.time() - start_time
			logger.info('runtime:%s', time.strftime('%H:%M:%S', time.gmtime(elapsed_time)))
	
	def write_csv(self, output_dir):
		now = datetime.datetime.now(tz=tz)
		file_name = output_dir + self.store_slug + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.csv'
		with open(file_name, mode='w') as output_file:
			output_file = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			output_file.writerow(["name", "price", "unit", "product_info", "product_image", "url"])
			for product in self.products:
				output_file.writerow([product.name, product.price, product.unit, product.product_info, product.product_picture, product.url])

	def write_workbook(self, workbook):
		worksheet = workbook.add_worksheet(self.store_slug)

		## Add a bold format to use to highlight cells.
		bold = workbook.add_format({'bold': 1})

		## Write some data headers.
		worksheet.write('A1', 'name', bold)
		worksheet.write('B1', 'price', bold)
		worksheet.write('C1', 'unit', bold)
		worksheet.write('D1', 'product_info', bold)
		worksheet.write('E1', 'product_image', bold)
		worksheet.write('F1', 'product_url', bold)

		 # Start from the first cell below the headers.
		row = 1
		col = 0

		for product in self.products:
			worksheet.write_string(row, col, product.name)
			worksheet.write_number(row, col + 1, product.price)
			worksheet.write_string(row, col + 2, product.unit)
			worksheet.write_string(row, col + 3, product.product_info)
			worksheet.write_string(row, col + 4, product.product_picture)
			worksheet.write_string(row, col + 5, product.url)
			row += 1
