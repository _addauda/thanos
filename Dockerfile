## Docker Base Image
FROM ubuntu:18.10

## Upgrade and install basic Python dependencies
RUN apt-get update \
	&& apt-get install -y \
	curl \
	bzip2 \
	gcc \
	libc6-dev \
	libssl-dev \
	libffi-dev \
	libpython3-dev \
	python3-pip \
	python3-dev \
	build-essential \
	chromium-browser \
	## Cleanup package files
	&& apt-get clean autoclean \
	&& apt-get autoremove -y \
	&& rm -rf /var/lib/{apt,dpkg,cache,log}/

ENV PYTHONIOENCODING=utf8

## Create app directory
WORKDIR /src

## Copying pip requirements and program files
COPY ./requirements.txt ./
COPY ./main.py ./
COPY ./crawlers/ ./crawlers/
COPY ./input_data/ ./input_data/
COPY ./output_data ./output_data/
COPY ./logs/ ./logs/

RUN pip3 install -r requirements.txt

## Copying chromedriver to a directory in path
COPY ./chromedriver-linux /usr/local/bin/chromedriver

## Filebeat
RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.2.0-linux-x86_64.tar.gz \
	&& tar xzvf filebeat-7.2.0-linux-x86_64.tar.gz && mv filebeat-7.2.0-linux-x86_64 filebeat

CMD ["/bin/bash", "-c", "python3 main.py -t"]