from dotenv import load_dotenv
load_dotenv()
import json
from crawlers.grocery_gateway import GroceryGateway
from crawlers.nofrills import NoFrills
from crawlers.metro import Metro
import logging
import sys
import time
import os
import slack
import datetime
import xlsxwriter
import pytz
tz = pytz.timezone('America/Toronto')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

APP_ENV = os.getenv("APP_ENV")
APP_INPUT_DIR = os.getenv("INPUT_DIR")
APP_OUTPUT_DIR = os.getenv("OUTPUT_DIR")
APP_LOG_DIR = os.getenv("LOG_DIR")
APP_INVENTORY_KEYWORDS_FILE_NAME = os.getenv("INVENTORY_KEYWORDS_FILE_NAME")
APP_TEST_INVENTORY_KEYWORDS_FILE_NAME = os.getenv("TEST_INVENTORY_KEYWORDS_FILE_NAME")
SLACK_CHANNEL_NAME = os.getenv("SLACK_CHANNEL_NAME")
SLACK_TOKEN = os.getenv("SLACK_TOKEN")

def load_inventory_keywords(file_path):
	logger.info('Loading inventory keywords file at %s', file_path)
	file_data = None
	with open(file_path) as json_file:  
		file_data = json.load(json_file)
	return file_data

def configure_logging(log_file_name):

		formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s","%Y-%m-%d %H:%M:%S")

		## File handler
		file_handler = logging.FileHandler(log_file_name)
		file_handler.setLevel(logging.INFO)
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)

		## Console handler
		console_handler = logging.StreamHandler()
		console_handler.setLevel(logging.INFO)
		console_handler.setFormatter(formatter)
		logger.addHandler(console_handler)

def slack_upload(file_name):
	now = datetime.datetime.now(tz=tz)
	client = slack.WebClient(token=SLACK_TOKEN)
	response = client.files_upload(channels=SLACK_CHANNEL_NAME, file=file_name, filename=file_name, filetype="xlsx", initial_comment="Thanos output for %s" % now.strftime('%A, %B %m at %I:%M%p'))

if __name__ == "__main__":

	now = datetime.datetime.now(tz=tz)

	workbook_name = APP_OUTPUT_DIR + 'thanos' + '-' + now.strftime('%Y-%m-%d %H:%M:%S') + '.xlsx'
	workbook = xlsxwriter.Workbook(workbook_name)

	configure_logging(APP_LOG_DIR + __name__ + '.log')

	## Loading keywords to be scraped
	keywords_file = None
	if (APP_ENV == "prod"): ## prod mode
		logger.info("running thanos in prod mode")
		keywords_file = APP_INPUT_DIR + APP_INVENTORY_KEYWORDS_FILE_NAME
	else : ## dev/test mode
		logger.info("running thanos in test mode")
		keywords_file = APP_INPUT_DIR + APP_TEST_INVENTORY_KEYWORDS_FILE_NAME

	inventory_keywords = load_inventory_keywords(keywords_file)

	if ("-gg" in sys.argv): ## Grocery Gateway
		gg = GroceryGateway(APP_LOG_DIR)
		logger.info("running crawler %s", gg.store_slug)
		gg.crawl(inventory_keywords)
		#gg.write_csv(APP_OUTPUT_DIR)
		gg.write_workbook(workbook)
		
	if ("-mt" in sys.argv): ## Metro
		mt = Metro(APP_LOG_DIR)
		logger.info("running crawler %s", mt.store_slug)
		mt.crawl(inventory_keywords)
		#mt.write_csv(APP_OUTPUT_DIR)
		mt.write_workbook(workbook)

	if ("-nf" in sys.argv): ## No Frills
		nf = NoFrills(APP_LOG_DIR)
		logger.info("running crawler %s", nf.store_slug)
		nf.crawl(inventory_keywords)
		#nf.write_csv(APP_OUTPUT_DIR)
		nf.write_workbook(workbook)

	logger.info("closing handler to workbook")
	workbook.close()
	
	if (APP_ENV == "prod"): ## prod mode
		logger.info("uploading to workbook:%s", workbook_name)
		slack_upload(workbook_name)