## Project thanos

Thanos is lightweight web crawler that extracts data from the following Canadian grocery chains:
    - Metro
    - No Frills
    -Grocery Gateway

It can be run from a Docker container and provides the following data:
    - Product name
    - Product price in imperial units (with support for metric units)
    - Product units
    - Product reference url
    - Product description (in a later release)

Requirements:
- chrome driver - needs to be in path
- install requirements.txt
- needs python 3

Running the program:
- running with -t flag put program in test mode
- run from main.py
- Example: "python3 main.py -t" runs the program in test mode

Structure:
- Store specifc web crawler are under the crawlers folder
- Input_data folder are objects the crawler will craw (JSON format)
- Output data folder is product information in csv file format
- Logs folder are program logs (.log)


- explain folder structure
- running with docker vs running local
